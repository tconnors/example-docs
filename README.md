# Example Docs

This is an example of what using GitLab for documentation is like.

View the [wiki](https://gitlab.com/tconnors/example-docs/-/wikis/home) for the documentation.